from django.db import models

    
class Kasir(models.Model):
    nama_kasir = models.TextField()
    id_kasir = models.CharField(max_length=100)
    tanggal = models.DateTimeField(auto_now_add=True)
    pemasukan = models.CharField(max_length=100)
    
    def _str_(self):
        return self.nama_kasir

class Menu(models.Model):
    kategori = models.TextField()
    jenis =  models.CharField(max_length=100)
    ukuran = models.CharField(max_length=100)
    harga = models.CharField(max_length=100)

    def _str_(self):
        return self.kategori
    
class Penjualan(models.Model):
    menu = models.ForeignKey('Menu', on_delete=models.CASCADE, null=True)
    jumlah_terjual = models.CharField(max_length=100)
    total = models.CharField(max_length=100)
    
    
    def _str_(self):
        return self.menu
    
class Stok_menu(models.Model):
    menu =models.ForeignKey('Menu', on_delete=models.CASCADE, null=True)
    stok = models.CharField(max_length=100)
    total_produksi = models.CharField(max_length=100)
    def _str_(self):
        return self.menu
class Keuangan(models.Model):
    kasir =models.ForeignKey('Kasir', on_delete=models.CASCADE, null=True)
    total_pemasukan = models.CharField(max_length=100)
    biaya_produksi = models.CharField(max_length=100)
    gaji_karyawan = models.CharField(max_length=100)
    pendapatan_bersih=models.CharField(max_length=100)
    def _str_(self):
        return self.kasir