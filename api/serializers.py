from rest_framework import serializers
from .models import Kasir,Menu,Penjualan,Stok_menu,Keuangan


class KasirSerializer(serializers.ModelSerializer):
    class Meta:
        model = Kasir
        fields = ["id", "nama_kasir","id_kasir","tanggal","pemasukan"]

class MenuSerializer(serializers.ModelSerializer):

    class Meta:
        model = Menu
        fields = ["id","minuman","jenis","ukuran","harga",]

class PenjualanSerializer(serializers.ModelSerializer):

    class Meta:
        model = Penjualan
        fields = ["id", "menu","jumlah_terjual","total"]

class Stok_menuSerializer(serializers.ModelSerializer):

    class Meta:
        model = Stok_menu
        fields = ["id", "menu","stok","total_produksi"]

class KeuanganSerializer(serializers.ModelSerializer):

    class Meta:
        model = Keuangan
        fields = ["id", "kasir","total_pemasukan","biaya_produksi","gaji_karyawan","pendapatan_bersih"]

