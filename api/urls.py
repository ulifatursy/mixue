from django.urls import path
from . import views

urlpatterns = [
     path("kasir/", views.KasirListCreate.as_view(), name="kasir-view-create"),
     path("kasir/<int:pk>/",views.KasirRetrieveUpdateDestroy.as_view(), name="update"),
     path("menu/", views.MenuListCreate.as_view(), name="menu-view-create"),
     path("menu/<int:pk>/",views.MenuRetrieveUpdateDestroy.as_view(), name="update"),
     path("penjualan/", views.PenjualanListCreate.as_view(), name="penjualan-view-create"),
     path("penjualan/<int:pk>/",views.PenjualanRetrieveUpdateDestroy.as_view(), name="update"),
     path("stok_menu/", views.Stok_menuListCreate.as_view(), name="stok_menu-view-create"),
     path("stok_menu/<int:pk>/",views.Stok_menuRetrieveUpdateDestroy.as_view(), name="update"),
     path("keuangan/", views.KeuanganListCreate.as_view(), name="keuangan-view-create"),
     path("keuangan/<int:pk>/",views.KeuanganRetrieveUpdateDestroy.as_view(), name="update"),
     ]