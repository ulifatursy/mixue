from django.shortcuts import render
from rest_framework import generics,status
from rest_framework.response import Response
from.models import Kasir,Menu,Penjualan,Stok_menu,Keuangan
from.serializers import KasirSerializer,MenuSerializer,PenjualanSerializer,Stok_menuSerializer,KeuanganSerializer

class KasirListCreate(generics.ListCreateAPIView):
    queryset = Kasir.objects.all()
    serializer_class = KasirSerializer

    def delete(self, request, *args, **kwargs):
        Kasir.objects.all().delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class KasirRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Kasir.objects.all()
    serializer_class = KasirSerializer
    lookup_field ="pk"

class MenuListCreate(generics.ListCreateAPIView):
    queryset = Menu.objects.all()
    serializer_class = MenuSerializer

    def delete(self, request, *args, **kwargs):
        Kasir.objects.all().delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class MenuRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Menu.objects.all()
    serializer_class = MenuSerializer
    lookup_field ="pk"

class PenjualanListCreate(generics.ListCreateAPIView):
    queryset = Penjualan.objects.all()
    serializer_class = PenjualanSerializer

    def delete(self, request, *args, **kwargs):
        Penjualan.objects.all().delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class PenjualanRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Penjualan.objects.all()
    serializer_class = PenjualanSerializer
    lookup_field ="pk"

class Stok_menuListCreate(generics.ListCreateAPIView):
    queryset = Stok_menu.objects.all()
    serializer_class = Stok_menuSerializer

    def delete(self, request, *args, **kwargs):
        Stok_menu.objects.all().delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class Stok_menuRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Stok_menu.objects.all()
    serializer_class = Stok_menuSerializer
    lookup_field ="pk"

class KeuanganListCreate(generics.ListCreateAPIView):
    queryset = Keuangan.objects.all()
    serializer_class = KeuanganSerializer

    def delete(self, request, *args, **kwargs):
        Keuangan.objects.all().delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class KeuanganRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Keuangan.objects.all()
    serializer_class = KeuanganSerializer
    lookup_field ="pk"